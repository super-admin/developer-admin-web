#!/usr/bin/env bash

npm run build:prod

docker rmi registry.cn-beijing.aliyuncs.com/jinan-teaching/dapeng-developer-web:latest

docker build -t registry.cn-beijing.aliyuncs.com/jinan-teaching/dapeng-developer-web:latest .

docker push registry.cn-beijing.aliyuncs.com/jinan-teaching/dapeng-developer-web:latest


