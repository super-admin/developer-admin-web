#标准的nginx镜像,我们需要基于标准的nginx镜像制作自己的镜像
FROM nginx
# 定义作者
MAINTAINER qiaoliang
# 将dist文件中的内容复制到 /usr/share/nginx/html/ 这个目录下面
COPY dist/  /usr/share/nginx/html/
#拷贝.conf文件到镜像下
COPY default.conf /etc/nginx/conf.d/
