#!/usr/bin/env bash

npm run build:prod

docker rmi registry.dapeng.lan/jinan-teaching/dapeng-developer-web:latest

docker build -t registry.dapeng.lan/jinan-teaching/dapeng-developer-web:latest .

docker push registry.dapeng.lan/jinan-teaching/dapeng-developer-web:latest


