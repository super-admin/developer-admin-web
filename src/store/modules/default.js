import { getOveralDict } from '@/api/dict'

const getDefaultState = () => {
  return {
    dict: []
  }
}

const state = getDefaultState()

const mutations = {
  SET_DICT: (state, dict) => {
    state.dict = dict
  }
}

const actions = {
  getOveralDict({ commit }) {
    return new Promise((resolve, reject) => {
      getOveralDict().then(response => {
        commit('SET_DICT', response.data)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
