import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import Cookies from 'js-cookie'

import Element from 'element-ui'

import elTableInfiniteScroll from 'el-table-infinite-scroll'

import 'element-ui/lib/theme-chalk/index.css'

import moment from 'moment'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

Vue.use(elTableInfiniteScroll)

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

// 定义全局时间格式过滤器
Vue.filter('dateFormat', function(daraStr, pattern = 'yyyy-MM-DD HH:mm:ss') {
  if (daraStr) {
    return moment(daraStr).format(pattern)
  }
  return ''
})

// 定义全局时间格式过滤器
Vue.filter('dateFormatAdd', function(daraStr, duration, pattern = 'yyyy-MM-DD HH:mm:ss') {
  return moment(new Date(new Date(daraStr).setMinutes(new Date(daraStr).getMinutes() + duration))).format(pattern)
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
