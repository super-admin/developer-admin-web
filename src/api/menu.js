import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/system/menu',
    method: 'get',
    params: query
  })
}

export function roleMenu(id) {
  return request({
    url: '/system/menu/role/' + id,
    method: 'get'
  })
}

export function tree() {
  return request({
    url: '/system/menu/tree',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/system/menu',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/system/menu/' + id,
    method: 'put',
    data
  })
}

export function deletes(id) {
  return request({
    url: '/system/menu/' + id,
    method: 'delete'
  })
}

export function parent() {
  return request({
    url: '/system/menu/parent',
    method: 'get'
  })
}
