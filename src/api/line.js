import request from '@/utils/request'

export function fetchList(id, query) {
  return request({
    url: '/data/assembly/line/' + id,
    method: 'get',
    params: query
  })
}

export function realTimeData(query) {
  return request({
    url: '/data/assembly/line/real-time-data',
    method: 'get',
    params: query
  })
}
