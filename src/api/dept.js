import request from '@/utils/request'

export function deptFetchList(query) {
  return request({
    url: '/system/dept/tree',
    method: 'get',
    params: query
  })
}

export function deptSearchList() {
  return request({
    url: '/system/dept/tree/search',
    method: 'get'
  })
}

export function deptSearch() {
  return request({
    url: '/system/dept/tree-users',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/system/dept',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/system/dept/' + id,
    method: 'put',
    data
  })
}

export function deletes(id) {
  return request({
    url: '/system/dept/' + id,
    method: 'delete'
  })
}

export function parent() {
  return request({
    url: '/system/dept/parent',
    method: 'get'
  })
}
