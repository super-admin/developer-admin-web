import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/info',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/remove',
    method: 'post'
  })
}

export function userFetchList(query) {
  return request({
    url: '/system/user',
    method: 'get',
    params: query
  })
}

export function info(id) {
  return request({
    url: '/system/user/' + id,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/system/user',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/system/user/' + id,
    method: 'put',
    data
  })
}

export function updateInfo(data) {
  return request({
    url: '/system/user/info',
    method: 'put',
    data
  })
}

export function updatePassword(data) {
  return request({
    url: '/system/user/password',
    method: 'put',
    data
  })
}

export function updateStatus(id) {
  return request({
    url: '/system/user/status/' + id,
    method: 'put'
  })
}

export function userCount() {
  return request({
    url: '/system/user/count',
    method: 'get'
  })
}
