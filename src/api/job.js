import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/system/quartz-job',
    method: 'get',
    params: query
  })
}
