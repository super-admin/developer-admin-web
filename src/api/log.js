import request from '@/utils/request'

export function logIndexList() {
  return request({
    url: '/system/log/index',
    method: 'get'
  })
}
