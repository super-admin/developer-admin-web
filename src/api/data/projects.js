import request from '@/utils/request'

export function updateOperationFetchList(query) {
  return request({
    url: '/data/update-operation',
    method: 'get',
    params: query
  })
}

export function updateOperationSearch(ids) {
  return request({
    url: '/data/update-operation/search',
    method: 'get',
    params: { systemIds: ids }
  })
}

export function create(data) {
  return request({
    url: '/data/update-operation',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/data/update-operation/' + id,
    method: 'put',
    data
  })
}

export function deletes(id) {
  return request({
    url: '/data/update-operation/' + id,
    method: 'delete'
  })
}
