import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/data/notice',
    method: 'get',
    params: query
  })
}

export function create(data) {
  return request({
    url: '/data/notice',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/data/notice/' + id,
    method: 'put',
    data
  })
}

export function deletes(id) {
  return request({
    url: '/data/notice/' + id,
    method: 'delete'
  })
}

export function noticeCount() {
  return request({
    url: '/data/notice/count',
    method: 'get'
  })
}
