import request from '@/utils/request'

export function templateFetchList(query) {
  return request({
    url: '/data/template',
    method: 'get',
    params: query
  })
}

export function templateSearch() {
  return request({
    url: '/data/template/search',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/data/template',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/data/template/' + id,
    method: 'put',
    data
  })
}

export function deletes(id) {
  return request({
    url: '/data/template/' + id,
    method: 'delete'
  })
}
