import request from '@/utils/request'

export function groupFetchList(query) {
  return request({
    url: '/data/group',
    method: 'get',
    params: query
  })
}

export function groupSearch() {
  return request({
    url: '/data/group/search',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/data/group',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/data/group/' + id,
    method: 'put',
    data
  })
}

export function deletes(id) {
  return request({
    url: '/data/group/' + id,
    method: 'delete'
  })
}
