import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/system/dict',
    method: 'get',
    params: query
  })
}

export function dataList(type) {
  return request({
    url: '/system/dict/data/' + type,
    method: 'get'
  })
}

export function createType(data) {
  return request({
    url: '/system/dict',
    method: 'post',
    data
  })
}

export function updateType(id, data) {
  return request({
    url: '/system/dict/' + id,
    method: 'put',
    data
  })
}

export function deleteType(type) {
  return request({
    url: '/system/dict/' + type,
    method: 'delete'
  })
}

export function createData(data) {
  return request({
    url: '/system/dict/data',
    method: 'post',
    data
  })
}

export function updateData(id, data) {
  return request({
    url: '/system/dict/data/' + id,
    method: 'put',
    data
  })
}

export function deleteData(id) {
  return request({
    url: '/system/dict/data/' + id,
    method: 'delete'
  })
}

export function getOveralDict() {
  return request({
    url: '/system/dict/overal',
    method: 'get'
  })
}

