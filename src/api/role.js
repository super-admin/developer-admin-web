import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/system/role',
    method: 'get',
    params: query
  })
}

export function userRole(id) {
  return request({
    url: '/system/role/user/' + id,
    method: 'get'
  })
}

export function list() {
  return request({
    url: '/system/role/normal',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/system/role',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/system/role/' + id,
    method: 'put',
    data
  })
}

export function deletes(id) {
  return request({
    url: '/system/role/' + id,
    method: 'delete'
  })
}
