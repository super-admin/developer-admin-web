import request from '@/utils/request'

export function systemFetchList(query) {
  return request({
    url: '/system',
    method: 'get',
    params: query
  })
}

export function systemSearch() {
  return request({
    url: '/system/search',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/system',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/system/' + id,
    method: 'put',
    data
  })
}

export function deletes(id) {
  return request({
    url: '/system/' + id,
    method: 'delete'
  })
}

export function systemCount() {
  return request({
    url: '/system/count',
    method: 'get'
  })
}
