import request from '@/utils/request'

export function friends(query) {
  return request({
    url: '/mit/friends',
    method: 'get',
    params: query
  })
}

export function read(data) {
  return request({
    url: '/mit/read',
    method: 'put',
    params: data
  })
}

export function list(acceptBy, query) {
  return request({
    url: '/mit/' + acceptBy,
    method: 'get',
    params: query
  })
}

export function count() {
  return request({
    url: '/mit/count',
    method: 'get'
  })
}
