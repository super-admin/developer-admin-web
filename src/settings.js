module.exports = {
  /** 系统标题 **/
  title: '大鹏开发者管理平台',
  /** 固定 Header **/
  fixedHeader: false,
  /** 开启 Tags-View **/
  tagsView: true,
  /** 侧边栏 Logo **/
  sidebarLogo: true,
  /** 系统主题设置 **/
  showSettings: true
}
